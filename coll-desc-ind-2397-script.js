;
/* Manipulate the digitool page to handle the laaarge letter collection of Christoph Jacob Trew
*
* Basically does 3 things:
* 1. Hide the "Alle Digitalisate anzeigen" button
* 2. Show a search slot for searching by shelfmark, sender or recipient/title
* 3. Show all letters that match the search terms
* 
* The list of person names is hardcoded, while the letters are
* fetched from Touchpoint's SRU/W API 
*
*/
(function(window, $, undefined) {
  
  //var itemsUrl = 'coll-desc-ind-2397-items.json';
  var itemsUrl = '/exlibris/dtl/u3_1/dtle/www_r_eng/icon/UBE/collection-desc/coll-desc-ind-2397-items.json';

  /** Html snippets that are to be inserted for search and item list
  */
  var snippet = {
    region: {
      GER: 
          '<td>'
        + '<div id="maskenball-search">'
        + '  <ul style="display: inline; list-style-type: none; margin-left: 1em;">'
        + '    <li><label for="maskenball-search-sender-slot">Briefe eingrenzen auf Personen:</label>'
        + '      <input type="text" id="maskenball-search-slot" value="Christoph Jacob Trew" style="width: 20em;"></input></li>'
        + '  </ul>'
        + '</div>'
        + '<div id="maskenball-itemlist">'
        + '  <p>Gefundene Briefe: <span id="maskenball-itemlist-amount"></span></p>'
        + '  <p id="maskenball-itemlist-emptylist" style="display: none;">Keine Briefe gefunden. Bitte ändern Sie Ihre Suche.</p>'
        + '  <ul style="list-style-type: none; padding: 0; display: -ms-flexbox; display: flex; -ms-flex-wrap: wrap; flex-wrap: wrap; justify-content: space-between; max-width: 93vw; ">'
        + '  </ul>'
        + '  <p id="maskenball-itemlist-progress">Lade Treffer...</p>'
        + '</div>'
        + '</td>',
      ENG:
          '<td>'
        + '<div id="maskenball-search">'
        + '  <ul style="display: inline; list-style-type: none; margin-left: 1em;">'
        + '    <li><label for="maskenball-search-sender-slot">Search letters by person:</label>'
        + '      <input type="text" id="maskenball-search-slot" value="Christoph Jacob Trew" style="width: 20em;"></input></li>'
        + '  </ul>'
        + '</div>'
        + '<div id="maskenball-itemlist">'
        + '  <p>Number of letters found: <span id="maskenball-itemlist-amount"></span></p>'
        + '  <p id="maskenball-itemlist-emptylist" style="display: none;">No letters found. Please broaden your search.</p>'
        + '  <ul style="list-style-type: none; padding: 0; display: -ms-flexbox; display: flex; -ms-flex-wrap: wrap; flex-wrap: wrap; justify-content: space-between; max-width: 93vw; ">'
        + '  </ul>'
        + '  <p id="maskenball-itemlist-progress">Loading results...</p>'
        + '</div>'
        + '</td>'
    },
    item: {
      GER:
          '<li class="maskenball-itemlist-item" style="font-size: small; clear: left; padding: 1em; height: 8em; width: 40em; -ms-flex: 1 0 auto;">'
        + '  <a target="_blank" href=""><img src="" style="float: left; max-height: 8em; margin-right: 1em;"></img></a>'
        + '  <p style="margin: 0px 0px 0.3em 0;">Titel: <span class="maskenball-item-title"></span></p>'
        + '  <p style="margin: 0px 0px 0.3em 0;">Autor: <span class="maskenball-item-author"></span></p>'
        + '  <p style="margin: 0px 0px 0.3em 0;">Signatur: <span class="maskenball-item-shelfmark"></span></p>'
        + '  <p style="margin: 0px 0px 0.3em 0;">Datierung: <span class="maskenball-item-date"></span></p>'
        + '</li>',
      ENG:
          '<li class="maskenball-itemlist-item" style="font-size: small; clear: left; padding: 1em; height: 8em; width: 40em; -ms-flex: 1 0 auto;">'
        + '  <a target="_blank" href=""><img src="" style="float: left; max-height: 8em; margin-right: 1em;"></img></a>'
        + '  <p style="margin: 0px 0px 0.3em 0;">Title: <span class="maskenball-item-title"></span></p>'
        + '  <p style="margin: 0px 0px 0.3em 0;">Author: <span class="maskenball-item-author"></span></p>'
        + '  <p style="margin: 0px 0px 0.3em 0;">Shelfmark: <span class="maskenball-item-shelfmark"></span></p>'
        + '  <p style="margin: 0px 0px 0.3em 0;">Date: <span class="maskenball-item-date"></span></p>'
        + '</li>'
    }
  };
  
  /** The page's language
  * Digitool seems to use the ISO 639-2 variant B...
  */
  var language; 
  var itemsBySender = {};
  var itemsByRecipient = {};
  var itemsByShelfmark = {};
  var senders = {};
  var recipients = {};
  var index = { termsList: [], terms: {}, shelfmark: {}, author: {}, title: {} };
  var currentSearchTerms = {};

  $().ready(function() {
    // initialize:
    // get the page's language, it is stored in a hidden span
    language = $('#current_language').text();
    // hide the button that links to the default item list (thing 1)
    $('#box_subcollections').hide();
    // add regions for search and result list
    $('#box_subcollections').after(snippet.region[language]);
    // update the list if selection changes
    $('#maskenball-search-slot').on('change keyup click', updateItemList);
    // load jquery ui css
    $('head').append('<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" type="text/css" />');
    // load items
    $.ajax({
      url: itemsUrl,
      dataType: 'json'
    })
    .done(function(itemsData) {
//console.log('items count:', itemsData.length);
      // cache the items for search
      for ( var i in itemsData) {
        var item = itemsData[i];
        itemsByShelfmark[item.shelfmark.toUpperCase()] = item;
        itemsBySender[item.author.toUpperCase()] = item;
        itemsByRecipient[item.recipient.toUpperCase()] = item;
        indexItem(item, i);
      }
      index.termsList = objectKeys(index.terms);
//console.log('index built:', index);
      // init sender and recipient search fields to expose autocomplete
      $.getScript('//code.jquery.com/ui/1.9.2/jquery-ui.js')
      .done(function() {
        $('#maskenball-search-slot')
          .autocomplete({
            // taken from: https://jqueryui.com/autocomplete/#multiple
            source: function(request, response) {
              var str = request.term.split(/ +/).pop().toUpperCase();
              var list = (str.length > 3) 
                ? arrayFilter(index.termsList, function(a) { return a.indexOf(str) != -1; })
                : arrayFilter(index.termsList, function(a) { return a.indexOf(str) == 0; });
//console.log('source', list, index);
              list = list.sort().slice(0,10);
              for (var i in list) {
                list[i] = list[i].substring(0, 1) + list[i].substring(1).toLowerCase();
                if (i == 10) list[i] = '...';
              }
              response(list);
            },
            focus: function() {
              // prevent value inserted on focus
              return false;
            },
            select: function( event, ui ) {
              var terms = this.value.split(/ +/);
              // remove the current input
              terms.pop();
              // add the selected item
              terms.push( ui.item.value );
              // add placeholder to get the comma-and-space at the end
              terms.push( "" );
//console.log('select', terms);
              this.value = terms.join( " " );
              return false;
            }
          });
      });
      $('#maskenball-search-slot').trigger('change');
    })
    .fail(function( jqXHR, textStatus, errorThrown ) { console.error(textStatus, errorThrown); });
    // end items.json load
  });
  
  
  /** helper function for filtering array elements
  * for compatibility as we cannot garantee that Array.filter() is available
  */
  function arrayFilter(array, filter) {
    var result = [];
    for (var i in array) {
      var v = array[i];
      if (filter(v, i, array) !== false) {
        result.push(v);
      }
    }
    return result;
  };

  /** helper function for creating a list of all keys of an object
  * for compatibility as we cannot garantee that Object.keys() is available
  */
  function objectKeys(object) {
    var keys = [];
    for (var k in object) {
      if (object.hasOwnProperty(k)) {
        keys.push(k);
      }
    }
    return keys;
  };


  /** Sorts out all duplicate items in an array.
  * Returns a new array where the first occurance of an item is kept and all subsequent occurances are removed.
  */  
  function arrayUnique(array) {
    return arrayFilter(array, function(v, i) { return i == array.indexOf(v); });
  }


  function indexItem(item, idx) {
    var words = arrayFilter(item.shelfmark.split(/[^a-zA-ZöäüÖÄÜß]+/), function(v) { return v != '' && v.charAt(0) == v.charAt(0).toUpperCase(); });
    for (var i in words) {
      var w = words[i].toUpperCase();
      if (!index.shelfmark[w]) index.shelfmark[w] = [];
      index.shelfmark[w].push(item.shelfmark);
      index.terms[w] = w;
    }
    var words = arrayFilter(item.author.split(/[^a-zA-ZöäüÖÄÜß]+/), function(v) { return v != '' && v.charAt(0) == v.charAt(0).toUpperCase(); });
    for (var i in words) {
      var w = words[i].toUpperCase();
      if (!index.author[w]) index.author[w] = [];
      index.author[w].push(item.shelfmark);
      index.terms[w] = w;
    }
    var words = arrayFilter(item.title.split(/[^a-zA-ZöäüÖÄÜß]+/), function(v) { return v != '' && v.charAt(0) == v.charAt(0).toUpperCase(); });
    for (var i in words) {
      var w = words[i].toUpperCase();
      if (!index.title[w]) index.title[w] = [];
      index.title[w].push(item.shelfmark);
      index.terms[w] = w;
    }
  }
  
  
  function updateItemList() {
    // make the search term set
    var searchTerms = {};
    var hasAdded = false;
    $.each($('#maskenball-search-slot').val().toUpperCase().split(/[^A-ZÖÄÜß]+/), function (i, v) {
      if (!index.terms[v]) return true;
      searchTerms[v] = v;
      if (!currentSearchTerms[v]) hasAdded = true;
    });
    var searchTermCount = objectKeys(searchTerms).length;
    // if something has changed in the search term set, then we update the list
    if (hasAdded || searchTermCount != objectKeys(currentSearchTerms).length) {
      // clear selection and indicate that we are working
      $('.maskenball-itemlist-item').remove();
      $('#maskenball-itemlist-emptylist').hide();
      $('#maskenball-itemlist-progress').show();
      $('#maskenball-itemlist-amount').hide();
      // de-register handlers for lazy loading
      $(window).off('scroll.maskenball');
    
//console.log('search for:', searchTerms);
      currentSearchTerms = searchTerms;
      var items = filterItems(searchTerms);
      // sort items by shelfmark to get conversations aligned correctly
// already sorted!?     items.sort();
//console.log('matching items:', items.length);
      if (items.length) {
        $('#maskenball-itemlist-amount')
          .text(items.length)
          .show();
        renderItems(items, 0, 90);
      }
      else {
        $('#maskenball-itemlist-emptylist').show();
      }
      $('#maskenball-itemlist-progress').hide();
    }
    else {
//console.log('no update', searchTerms, currentSearchTerms);
    }
  };


  function filterItems(searchTerms) {
    // by default, we 'and' all search terms to get a smaller result set
    var andOr = arguments.length > 1 ? arguments[1] : 'and';
    var dicts = [index.shelfmark, index.author, index.title];
    // compile the result list of matching items
    var matchedItems = [];
    for (var i in dicts) {
      var dict = dicts[i];
      var sets = [];
      for (var j in searchTerms) {
//console.log('search in:', i, 'with:', searchTerms[j], 'and:', andOr);
        var term = searchTerms[j];
        if (!!dict[term]) sets.push(dict[term]);
      }
      sets = combineResultSets(andOr, sets);
      matchedItems = combineResultSets('or', [matchedItems, sets]);
    }
    // return if we found something, otherwise do a more relaxed search
    if (matchedItems.length == 0 && andOr != 'or') {
      // if we didn't match any items, we try to 'or' the search terms and thus
      // broaden our search unless we are already in or mode
      return filterItems(searchTerms, 'or');
    }
    return matchedItems;
  };


  function combineResultSets(andOr, sets) {
    if (sets.length == 0) {
      return [];
    }
    else if (sets.length == 1) {
      return sets[0];
    }
    // from here on we have multiple sets, ie. sets.length > 1
    else {
      // do an intersection (andOr = and) or union (andOr = or) of sets
      if (andOr == 'and') {
        return sets.reduce(function(a, b) { return arrayFilter(a, function(v) { return b.indexOf(v) != -1; }); });
      } 
      else if (andOr = 'or') {
        return arrayUnique(sets.reduce(function(a, b) { return a.concat(b); }));
      }
      else {
        console.error('bad operation argument: ' + andOr);
        return [];
      }
    }
  };


  function renderItems(shelfmarkList, offset, amount) {
    var limit = Math.min(offset + amount, shelfmarkList.length);
//console.log('render', shelfmarkList.length);
    for (var i = offset; i < limit; i++) {
      var item = itemsByShelfmark[shelfmarkList[i].toUpperCase()];
      if (!item) {
        console.error('unknown shelfmark: ' + shelfmarkList[i]);
        continue;
      }
      var $item = $(snippet.item[language]);
      $item.find('a').attr('href', buildItemLink(item.pid));
      $item.find('img').attr('src', buildItemThumb(item.pid));
      $item.find('.maskenball-item-title').text(item.title);
      $item.find('.maskenball-item-author').text(item.author);
      $item.find('.maskenball-item-shelfmark').text(item.shelfmark);
      $item.find('.maskenball-item-date').text(item.date);
      $('#maskenball-itemlist ul').append($item);
    }
    $('.maskenball-itemlist-item').show();
//console.log('rendered', offset, limit);
    // make a lazy loader
    if (limit < shelfmarkList.length) {
      $(window).on('scroll.maskenball', function() {
//console.log('dada');        
        if ($(window).scrollTop() + $(window).height() + 100 >= $(document).height()) {
//console.log('daiida');        
          $(window).off('scroll.maskenball');
          renderItems(shelfmarkList, limit, amount);
        }
      });
    }
  };
 

  function buildItemLink(pid) {
    return "http://digital.bib-bvb.de/webclient/DeliveryManager?pid=" + pid + "&custom_att_2=direct";
  }
 

  function buildItemThumb(pid) {
    return "http://digipool.bib-bvb.de/bvb/delivery/tn_stream.fpl?pid=" + pid;
  }

})(window, jQuery);
