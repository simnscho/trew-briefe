#!/bin/bash


echo "Brief;Datum" > zeit.csv
xmlstarlet sel -T -N marc="http://www.loc.gov/MARC21/slim" \
  -t -m '//marc:datafield[@tag="264"]' \
     -v '../marc:controlfield[@tag="001"]' -o ';' \
     -v 'marc:subfield[@code="c"]' -n  ../marcxml/* |
perl -pe 's/[\[\]]//g; s/^(BV\d+;).*?(\d{4})?.*$/$1$2/;' |
sort -u >> zeit.csv


