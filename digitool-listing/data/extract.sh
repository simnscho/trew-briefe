#!/bin/bash

echo "Brief;GND;Name" > ort.csv
xmlstarlet sel -T -N marc="http://www.loc.gov/MARC21/slim" \
  -t -m '//marc:datafield[@tag="751"]' \
     -v '../marc:controlfield[@tag="001"]' -o ';' \
     -v 'substring-after(marc:subfield[@code="0"], ")")' -o ';' \
     -v 'marc:subfield[@code="a"]' -n  ../marcxml/* |
sort -u |
grep -v ';;' >> ort.csv

echo "Brief;Datum" > zeit.csv
xmlstarlet sel -T -N marc="http://www.loc.gov/MARC21/slim" \
  -t -m '//marc:datafield[@tag="264"]' \
     -v '../marc:controlfield[@tag="001"]' -o ';' \
     -v 'marc:subfield[@code="c"]' -n  ../marcxml/* |
perl -pe 's/[\[\]]//g; s/^(BV\d+;).*?(\d{4})?.*$/$1$2/;' |
sort -u |
grep -v ';$' >> zeit.csv

echo "Brief;GND;Name" > autor.csv
xmlstarlet sel -T -N marc="http://www.loc.gov/MARC21/slim" \
  -t -m '//marc:datafield[@tag="100" or @tag="110" or (@tag="700" and marc:subfield[@code="e"] = "Verfasser")]' \
     -v '../marc:controlfield[@tag="001"]' -o ';' \
     -v 'substring-after(marc:subfield[@code="0"], ")")' -o ';' \
     -v 'marc:subfield[@code="a"]' -n  ../marcxml/* |
sort -u |
grep -v ';;' >> autor.csv

echo "Brief;GND;Name" > adressat.csv
xmlstarlet sel -T -N marc="http://www.loc.gov/MARC21/slim" \
  -t -m '//marc:datafield[@tag="700" and marc:subfield[@code="e"] != "Adressat"]' \
     -v '../marc:controlfield[@tag="001"]' -o ';' \
     -v 'substring-after(marc:subfield[@code="0"], ")")' -o ';' \
     -v 'marc:subfield[@code="a"]' -n  ../marcxml/* |
sort -u |
grep -v ';;' >> adressat.csv


