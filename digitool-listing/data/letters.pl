#!/usr/bin/perl

while (<>) {
  chomp;
  my ($brief, $gnd, $name) = split /;/;
  $gnd{$gnd} = [] unless exists $gnd{$gnd};
  push @{$gnd{$gnd}}, $brief;
  $name{$gnd} = $name;
}

#foreach my $gnd (keys %gnd) {
#  my @briefe = @{$gnd{$gnd}};
#  for (my $i = 1; $i < scalar @briefe; $i++) {
#    for (my $j = 0; $j < $i; $j++) {
#      print "$briefe[$i];$briefe[$j];$gnd;$name{$gnd}\n";
#    }
#  }
#}

foreach my $gnd (keys %gnd) {
  my @briefe = @{$gnd{$gnd}};
  print join(';', @briefe) . "\n";
}
