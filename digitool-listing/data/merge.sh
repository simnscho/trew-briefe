#!/bin/bash


cat autor.csv adressat.csv ort.csv zeit.csv |
cut -d';' -f 1 |
sort -u > bv.merge.tmp

echo "Letter;Source;SourceName;Target;TargetName;FromLocation;FromLocationName;FromYear" > merge.csv

for BV in $(cat bv.merge.tmp); do
  echo "processing $BV..."
  SOURCE=`grep "$BV" autor.csv | cut -d";" -f 2`
  SOURCENAME=`grep "$BV" autor.csv | cut -d";" -f 3`
  TARGET=`grep "$BV" adressat.csv | cut -d";" -f 2`
  TARGETNAME=`grep "$BV" adressat.csv | cut -d";" -f 3`
  FROMLOCATION=`grep "$BV" ort.csv | cut -d";" -f 2`
  FROMLOCATIONNAME=`grep "$BV" ort.csv | cut -d";" -f 3`
  FROMYEAR=`grep "$BV" zeit.csv | cut -d";" -f 2`
  SOURCE=`grep "$BV" autor.csv | cut -d";" -f 2`
  echo "$BV;$SOURCE;$SOURCENAME;$TARGET;$TARGETNAME;$FROMLOCATION;$FROMLOCATIONNAME;$FROMYEAR" >> merge.csv
done




#
#cat autor.csv adressat.csv ort.csv zeit.csv |
#cut -d';' -f 1 |
#sort | uniq -c |
#grep -v '^ *4' |
#grep -oP 'BV\d+' > exclude.tmp
#
#grep -f exclude.tmp autor.csv    | perl -pe 's/;/;A;/;'  > merge.tmp
#grep -f exclude.tmp adressat.csv | perl -pe 's/;/;B;/;' >> merge.tmp
#grep -f exclude.tmp ort.csv      | perl -pe 's/;/;O;/;' >> merge.tmp
#grep -f exclude.tmp zeit.csv     | perl -pe 's/;/;Z;/;' >> merge.tmp
#
#sort merge.tmp | tee merge.tmp2 |
#perl -F';' -ane '$f{$F[1]} = $F[2]; if ($F[1] eq "Z") { print "$f[A]$f[B]$f[O]$f[Z]\n"; %f = (); }' > merge.csv
#

