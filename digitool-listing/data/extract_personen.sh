#!/bin/bash

echo "Brief;GND;Name" > personen.csv
xmlstarlet sel -T -N marc="http://www.loc.gov/MARC21/slim" \
  -t -m '//marc:datafield[@tag="700" or @tag="100"]' \
     -v '../marc:controlfield[@tag="001"]' -o ';' \
     -v 'substring-after(marc:subfield[@code="0"], ")")' -o ';' \
     -v 'marc:subfield[@code="a"]' -n  ../marcxml/* |
sort -u |
grep -v ';;' >> personen.csv


