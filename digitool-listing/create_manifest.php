#!/usr/bin/php
<?php

$template_manifest = '
{
  "@context": "http://iiif.io/api/presentation/2/context.json",
  "@id": "http://dh1.ub.fau.de/trew-mirador/data/iiif/%{itemId}.json",
  "@type": "sc:Manifest",
  "description": "%{shelfmark}; %{author}: %{title}",
  "label": "%{shelfmark}; %{author}: %{title}",
  "metadata": [{
      "label": "Institution",
      "value": "Universitätsbibliothek Erlangen-Nürnberg"
    }, {
      "label": "Signatur",
      "value": "%{shelfmark}"
    }
  ],
  "rendering": {
    "@id": "https://ub.fau.de",
    "format": "text/html",
    "label": "Further information"
  },
  "sequences": [{
      "@id": "http://dh1.ub.fau.de/trew-mirador/data/iiif/%{itempid}/sequence",
      "@type": "sc:Sequence",
      "viewingHint": "individuals",
      "canvases": []
    }
  ]
}';
      
$template_canvas = '
{
  "@id": "http://dh1.ub.fau.de/trew-mirador/data/iiif/%{imagepid}/canvas",
  "@type": "sc:Canvas",
  "label": "%{label}",
  "width": "%{width}",
  "height": "%{height}",
  "images": [{
      "@id": "http://dh1.ub.fau.de/trew-mirador/data/iiif/%{imagepid}/image",
      "@type": "oa:Annotation",
      "motivation": "sc:painting",
      "on": "http://dh1.ub.fau.de/trew-mirador/data/iiif/%{imagepid}/canvas",
      "resource": {
        "@id": "http://dh1.ub.fau.de:8182/iiif/2/%{imagepid}/full/full/0/default.jpg",
        "@type": "dctypes:Image",
        "format": "image/jpeg",
        "width": "%{width}",
        "height": "%{height}",
        "service": {
          "@context": "http://iiif.io/api/image/2/context.json",
          "@id": "http://dh1.ub.fau.de:8182/iiif/2/%{imagepid}",
          "profile": "http://iiif.io/api/image/2/level2.json"
        }
      }
    }
  ]
}';

$pid_path_prefix = 'pid';
$items_file = '../coll-desc-ind-2397-items.json';

$items = json_decode(file_get_contents($items_file));

$itemsToProcess = array_filter(explode("\n", file_get_contents('php://stdin')));
$itemsToProcess = array_combine($itemsToProcess, $itemsToProcess);
echo "processing " . count($itemsToProcess) . " items\n";

foreach ($items as $item) {
  if (! isset($itemsToProcess[$item->shelfmark])) continue;
  echo "processing item $item->shelfmark...\n";

  $pid = $item->pid;
  $structure = json_decode(file_get_contents("$pid_path_prefix/$pid.json"));
  if (! $structure) {
    echo "no structural metadata found. skipping.\n";
    continue;
  }

  $itemId = preg_replace('/^_*(.*?)_*$/u', '$1', preg_replace('/[^-a-zA-Z0-9]+/u', '_', $item->shelfmark));

  $manifest = json_decode(strtr($template_manifest,
    [
      '%{shelfmark}' => $item->shelfmark,
      '%{itemId}' => $itemId,
      '%{itempid}' => $pid,
      '%{author}' => $item->author,
      '%{title}' => $item->title,
    ]
  ));

  foreach ($structure[0]->children as $image) {
    $imagepid = $image->attr->pid;
    if (!$imagepid) {
      echo "missing image pid!\n";
      continue;
    }
    $info = json_decode(file_get_contents("http://dh1.ub.fau.de:8182/iiif/2/$imagepid/info.json"));
    if (!$info) {
      echo "cannot access info.json for pid $imagepid!\n";
      continue;
    }
    
    $canvas = json_decode(strtr($template_canvas,
      [
        '%{label}' => $image->data,
        '%{imagepid}' => $imagepid,
        '%{width}' => $info->width,
        '%{height}' => $info->height,
      ]
    ));

    $manifest->sequences[0]->canvases[] = $canvas;
#    $seq = $manifest->sequences[0];
#    $seq->canvases[] = $canvas;

  }
  
  file_put_contents("iiif/$itemId.json", json_encode($manifest,  JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
}
