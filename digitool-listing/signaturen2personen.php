#!/usr/bin/php
<?php

$sf = str_replace("\n", "", explode("\n", file_get_contents("signaturen.txt"))); 
$pf = str_replace("\n", "", explode("\n", file_get_contents("personen.txt")));
$fail = [];
$unamb = [];
$amb = [];
foreach ($sf as $s) {
  $pc = $pf;
  $found_something = 0;
  foreach (preg_split("![ _]+!u", $s, -1, PREG_SPLIT_NO_EMPTY) as $sw) {
    foreach ($pc as $i => $pn) {
      $pn = strtoupper($pn);
      #$pn = preg_split('![ ,]+!u', strtoupper($pn), -1, PREG_SPLIT_NO_EMPTY);
      if (!preg_match("!\\b$sw\\b!u", $pn)) { 
        unset($pc[$i]);
        $found_something++;
      }
    }
  }
  if (!$found_something || empty($pc)) {
    $fail[] = $s;
  }
  else {
    $pi = [];
    foreach ($pc as $p) {
      list($id,) = explode('|', $p);
      $pi[$id] = $id;
    }
    if (count($pi) == 1) {
      $unamb[] = "$s:" . join(';', $pc);
    }
    else {
      $amb[] = "$s:" . join(';', $pc);
    }
  }
} 

file_put_contents('signaturen_personen_fail.txt', join("\n", $fail));
file_put_contents('signaturen_personen_unamb.txt', join("\n", $unamb));
file_put_contents('signaturen_personen_amb.txt', join("\n", $amb));

