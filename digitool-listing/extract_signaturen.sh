#!/bin/bash

xmlstarlet sel -T -N marc="http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd" -t -v '//marc:datafield[@tag="983"]/marc:subfield[@code="j"]' -n  marcxml/* |
perl -pe 's!H62/TREWBR ([^\[]*)(\[.*)?!$1!;' |
sort -u > signaturen.txt


