#!/usr/bin/perl

print "[\n";
while (<>) {
  next if /^$/;
  chomp;
  ($gnd, $label) = split /\|/;
  if ($gnd ne $lastgnd) {
    print "] },\n" if $lastgnd;
    print "{\"gnd\": \"$gnd\", \"prefLabel\": \"$label\", \"allLabels\": [\"$label\"";
    $lastgnd = $gnd;
  }
  else {
    print ", \"$label\"";
  }
}
print "] }\n]";
