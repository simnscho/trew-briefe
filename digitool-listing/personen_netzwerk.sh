#!/bin/bash

xmlstarlet sel -T -N marc="http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd" -t -m '//marc:datafield[@tag="109" or @tag="100" or @tag="110" or @tag="119" or @tag="700" or @tag="709"]' -f -o ':' -v 'marc:subfield[@code="0"]' -o '|' -v 'marc:subfield[@code="a"]' -n  marcxml/* |
php -r '$f = file_get_contents("php://stdin"); $f = preg_replace("/[\u{98}\u{9c}]/u", "", $f); $f = preg_replace("/\(DE-588\)/u", "", $f); echo $f;' |
sort -u > personen.txt

xmlstarlet sel -T -N marc="http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd" -t -m '//marc:datafield[@tag="100" or @tag="110" or (@tag="700" and marc:subfield[@code="e"] = "Verfasser")]' -f -o ':' -v '//marc:datafield[@tag="983"]/marc:subfield[@code="j"]' -o '|' -v 'marc:subfield[@code="0"]' -o ';' -v 'marc:subfield[@code="a"]' -n  marcxml/* |
php -r '$f = file_get_contents("php://stdin"); $f = preg_replace("/[\u{98}\u{9c}]/u", "", $f); $f = preg_replace("/\(DE-588\)/u", "", $f); $f = preg_replace("/\\|;/u", "|xxx;", $f); echo $f;' |
sort -u > personen_autor.txt

xmlstarlet sel -T -N marc="http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd" -t -m '//marc:datafield[@tag="700" and marc:subfield[@code="e"] != "Verfasser"]' -f -o ':' -v '//marc:datafield[@tag="983"]/marc:subfield[@code="j"]' -o '|' -v 'marc:subfield[@code="0"]' -o ';' -v 'marc:subfield[@code="a"]' -o '>>' -v 'marc:subfield[@code="e"]' -n  marcxml/* |
php -r '$f = file_get_contents("php://stdin"); $f = preg_replace("/[\u{98}\u{9c}]/u", "", $f); $f = preg_replace("/\(DE-588\)/u", "", $f); $f = preg_replace("/\\|;/u", "|xxx;", $f); echo $f;' |
sort -u > personen_sonst.txt


