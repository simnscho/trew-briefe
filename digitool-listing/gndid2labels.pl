#!/bin/bash

echo "{"
while read -r gnd; do
  curl "http://d-nb.info/gnd/$gnd" | rapper -i turtle -o ntriples > gndid2labels.tmp
  echo "\"$gnd\": { \"gnd\": \"$gnd\", "
  grep -oP '(?<=preferredNameForThePerson> )"[^"]+"' <gndid2labels.tmp | perl -pe 's/^(.*)$/  "prefLabel": $1,/;'
  echo "  \"altLabels\": ["
  grep -oP '(?<=preferredNameForThePerson> )"[^"]+"' <gndid2labels.tmp | perl -pe 's/^(.*)$/    $1,/;'
  echo "  ] },"
done 

echo "}"
