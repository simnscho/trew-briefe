#!/bin/bash

MARCDIR=$1

if [ "x$MARCDIR" = "x" ]; then
  echo "Syntax: cat file_with_bv_numbers_one_per_line | $0 dir_in_which_files_are_downloaded"
  exit 1
fi  

mkdir -p $MARCDIR

echo "start fetching records at " `date`

c=0
for bvid in $(cat -); do
  ((c++))
  if [ "x$bvid" != "x" ]; then
    echo "processing line $c with record $bvid"
    curl "http://bvbr.bib-bvb.de:8991/aleph-cgi/oai/oai_opendata.pl?verb=GetRecord&metadataPrefix=marc21&identifier=$bvid" > "$MARCDIR/$bvid.xml"
  fi
done

echo "finished after $c lines at " `date`

