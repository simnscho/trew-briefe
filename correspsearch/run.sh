#!/bin/bash

rm briefsammlung_trew.cmif.xml 
rm contents.csv
mv geonames.csv geonames.csv.$$
grep -v unknown geonames.csv.$$ | cat geonames_manual.csv - > geonames.csv

cp template.cmif.xml briefsammlung_trew.cmif.xml

TIMESTAMP=`date "+%Y%m%dT%H%M%S"`
python3 correspSearch.py -i bvnummern_sort.txt -o briefsammlung_trew.cmif.xml -mf marcxml/ -rt

cp briefsammlung_trew.cmif.xml briefsammlung_trew.$TIMESTAMP.cmif.xml 
mv contents.csv contents.$TIMESTAMP.csv
mv geonames.csv geonames.$TIMESTAMP.csv
mv geonames.csv.$$ geonames.csv

