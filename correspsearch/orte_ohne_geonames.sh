#!/bin/bash

xmlstarlet sel -N "tei=http://www.tei-c.org/ns/1.0" -t -m '//tei:placeName[not(@ref)]' -v '.'  -n < $1 | sort -u > orte_ohne_geonames.txt

xmlstarlet sel -N "tei=http://www.tei-c.org/ns/1.0" -t -m '//tei:placeName[not(@ref)]' -v '.' -o ':' -v '../../@ref' -n < briefe.tei.xml  > orte_ohne_geonames_bvnr.txt

