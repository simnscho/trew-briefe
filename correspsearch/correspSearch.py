#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "Raul C. Sîmpetru"
__version__ = "1.0.1"
__email__ = "raul.simpetru@fau.de"

import csv
import datetime
import os
import re
import time
import uuid
import argparse
from tqdm import tqdm
from typing import Tuple, Any, List

import requests
from lxml import etree as et
from SPARQLWrapper import SPARQLWrapper, JSON

# TODO: CHANGE THIS
author_of_file = "Handschriftenabteilung der Universitätsbibliothek " \
                 + "Erlangen-Nürnberg"
email_of_author = "ub-handschriften@fau.de"
idno_url = "https://unknown.com"
titleStmp_title = ""


MARC_URL = "http://bvbr.bib-bvb.de:8991/aleph-cgi/oai/oai_opendata.pl?verb" \
           + "=GetRecord&metadataPrefix=marc21&identifier="

NORMDATA_URL = "http://d-nb.info/gnd/"

UNKNOWN_NAME_NORMDATA_URL = "http://correspSearch.net/unknown"

URN_URL = "https://nbn-resolving.org/"

GEONAMES_URL = "http://www.geonames.org/"

# TODO: CHANGE THIS TO YOUR ACCOUNT
# GEONAMES_USERNAME = "USERNAME"
#
# GEONAMES_API_URL = "http://api.geonames.org/search?q={" \
#                    + "}&maxRows=1&style=SHORT&username=" + GEONAMES_USERNAME

WIKIDATA_QUERY_URL_PART1 = "https://query.wikidata.org/sparql?query=SELECT%20" \
                           + "%3FGEONAME_ID%20WHERE%20%7B%0A%20%20%3Fcity" \
                           + "%20wdt%3AP227%22%"
WIKIDATA_QUERY_URL_PART2 = "%22%3B%0A%20%20%20%20wdt%3AP1566%20%3FGEONAME_ID.%0A%7D%0ALIMIT%201"

BRIEFSAMMLUNG_TREW_URL = "https://ub.fau.de/bibliotheken-sammlungen/digitale-sammlungen/"

LICENCE_URL = "https://creativecommons.org/licenses/by/null/"
LICENCE_TEXT = "CC0"

TEI_URL = "http://www.tei-c.org/ns/1.0"

UB_URL = "https://ub.fau.de/"
UB_NAME = "Universitätsbibliothek Erlangen-Nürnberg"

# used to save geonames entries so the API doesn't need to be asked every time
GEONAMES_CSV_FILE_NAME = "geonames.csv"

# used to save UUID entries so every CMIF File has one static UUID when updated
UUID_CSV_FILE_NAME = "uuids.csv"

# used to save all added marc files to a tei file
TEI_CONTENTS_CSV_FILE_NAME = "contents.csv"

# used to store the location of letters on the hard drive
marc_letters_folder_name = ""
letters_on_drive = []

# used to see if type needs to be removed
removeType = False

# used to see if user wishes to have a verbose output
verbose = False


def get_geonames_id(location_gnd_id: str, location_name: str) -> str:
	"""
	Returns the id of the location inputted from the geonames xml dump.
	If nothing is found return none
	
	:param location_gnd_id: gnd id of the location
	:param location_name: name of the location used if the gnd_id does not work
	:returns: geonames id url OR an empty string
	"""
	
	# try to remove anything in the location_name after the first space
	try:
		location_name_for_wikidata = location_name.split(" ")[0].replace(",",
		                                                                 "")
	except:
		location_name_for_wikidata = location_name
	
	time.sleep(1)
	
	endpoint_url = "https://query.wikidata.org/sparql"
	
	query_part1 = """SELECT ?geoname_id WHERE {
            ?human_settlement wdt:P227 """
	query_part2 = """;
			wdt:P1566 ?geoname_id.
			}
			LIMIT 1"""
	
	backup_query_part1 = """SELECT ?geoname_id WHERE {
    ?human_settlement ?human_settlementLabel """
	
	query = query_part1 + "\"" + location_gnd_id + "\"" + query_part2
	backup_query = backup_query_part1 + "\"" + location_name_for_wikidata + "\"" + query_part2
	
	sparql = SPARQLWrapper(endpoint_url)
	sparql.setQuery(query)
	sparql.setReturnFormat(JSON)
	
	# try your best to find a geonames_id
	try:
		# this should always be the geonames_id
		output = sparql.query().convert()["results"]["bindings"][0][
			"geoname_id"]["value"]
		
		if not output:
			return ""
		else:
			return output
	except:
		sparql = SPARQLWrapper(endpoint_url)
		sparql.setQuery(backup_query)
		sparql.setReturnFormat(JSON)
		try:
			output = sparql.query().convert()["results"]["bindings"][0][
				"geoname_id"]["value"]
			if not output:
				return ""
			else:
				return output
		except:
			return ""
		
		# if nothing on wikidata works use the geonames api
		# url = GEONAMES_API_URL.format(location_name)
		# response = requests.get(url)
		#
		# geonames = et.fromstring(response.content)
		#
		# try:
		# 	# should always be the geonameId element
		# 	return geonames[1][4].text
		# except:
		# 	return ""


def convert_date_to_iso(date: str) -> Tuple[str, str]:
	"""
	Converts the given string to an iso 8601 compliant date format
	
	:param date: not formatted date string
	:returns: iso 8601 compliant date string and if there is a wrong date format
			  what should be in the text field (
			  01.13.1970 -> 01.12.1970, 01.13[sic!].1970 ;
			  31.02.1970 -> 28.02.1970, 31[sic!].02.1970;
			  01.01.1970 -> 01.01.1970, "";
			  34.13.1970 -> 31.12.1970, 34[sic!].13[sic!].1970
			  )
	"""
	
	# remove all letters and spaces from the given string
	dateWithoutAnyLetters = ""
	for letter in date:
		if not (letter.isalpha()):
			dateWithoutAnyLetters += letter
	
	dateWithoutAnyLettersAndSpaces = ''.join(dateWithoutAnyLetters.split())
	
	# get the dd, mm, and yyyy parts to clean them up
	dateParts = dateWithoutAnyLettersAndSpaces.split(".")
	
	# remove everything that is not a digit
	for i in range(len(dateParts)):
		dateParts[i] = ''.join(c for c in dateParts[i] if c.isdigit())
	
	# only accepted formats are yyyy.mm.dd yyyy.mm or yyyy so if anything is
	# missing it needs to be formatted correctly
	# also for some reason some dates make no sense like the 15.19.1725
	try:
		if len(dateParts) == 1 and len(dateParts[0]) == 4:
			return dateParts[0], ""
		elif not dateParts[-1]:
			return "", ""
		elif not dateParts[-2]:
			return dateParts[-1], ""
		elif not dateParts[-3]:
			return "{}-{}".format(
				dateParts[-1],
				dateParts[-2].zfill(2)
			), ""
		else:
			# uncomment following part if you wish to check for valid dates
			
			days_per_month_no_leap = {
				1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31,
				8: 31, 9: 30, 10: 31, 11: 30, 12: 31
			}
			
			days_per_month_leap = {
				1: 31, 2: 29, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31,
				8: 31, 9: 30, 10: 31, 11: 30, 12: 31
			}

			# some date validity checking
			textEntry = ""
			wrongPosition = ""
			wrongMonth = ""
			wrongDay = ""
			
			if int(dateParts[-2]) > 12:
				wrongMonth = dateParts[-2]
				dateParts[-2] = "12"
				wrongPosition = "m"
			
			# calculate leap year
			year = int(dateParts[-1])
			leap = False
			
			if year % 4 == 0:
				if year % 100 != 0:
					leap = True
				elif year % 400 == 0:
					leap = True
					
			if leap:
				if int(dateParts[-3]) > days_per_month_leap[int(dateParts[-2])]:
					wrongDay = dateParts[-3]
					dateParts[-3] = str(days_per_month_leap[int(dateParts[-2])])
					
					if not wrongPosition:
						wrongPosition = "d"
					else:
						wrongPosition += "d"
			
			else:
				if int(dateParts[-3]) > days_per_month_no_leap[int(dateParts[-2])]:
					wrongDay = dateParts[-3]
					dateParts[-3] = str(days_per_month_no_leap[int(dateParts[-2])])
					
					if not wrongPosition:
						wrongPosition = "d"
					else:
						wrongPosition += "d"
			
			if wrongPosition == "m":
				textEntry = "{}.{}[sic!].{}".format(
					dateParts[-3].zfill(2),
					wrongMonth,
					dateParts[-1]
				)
			elif wrongPosition == "d":
				textEntry = "{}[sic!].{}.{}".format(
					wrongDay,
					dateParts[-2].zfill(2),
					dateParts[-1]
				)
			elif wrongPosition == "md":
				textEntry = "{}[sic!].{}[sic!].{}".format(
					wrongDay,
					wrongMonth,
					dateParts[-1]
				)
			
			return "{}-{}-{}".format(
				dateParts[-1],
				dateParts[-2].zfill(2),
				dateParts[-3].zfill(2)
			), textEntry
	except:
		return "", ""


def get_information_from_marc(marc_url: str, geo_names: dict) -> dict:
	"""
	Creates a dictionary with all needed information from a marc file url.
	
	The geo_names dictionary is used so the script doesn't need to call
	the API every time e.g Nuremberg appears as a location
	
	:param marc_url: url of the marc file
	:param geo_names: dictionary containing geonames information
	:returns: dictionary containing all important information for a tei file
	"""
	
	output = {
		'author name': "Unknown",  # Unknown is default just in case
		'author norm data': UNKNOWN_NAME_NORMDATA_URL,
		'date': "",
		'receiver name': "Unknown",  # Unknown is default just in case
		'receiver norm data': UNKNOWN_NAME_NORMDATA_URL,
		'location name': "",
		'location norm data': "",
		'letter type': "Sonstiges",  # made default just in case
		'urn': "",
	}
	
	# see if letter is in marc folder (folder used for storing the trew
	# marc letters so the server needn't be involved
	
	# if not ask the server
	# OAI_PMH = name of root in MARC-21 files
	if marc_url.replace(MARC_URL, "") + ".xml" in letters_on_drive:
		if verbose:
			print(
				"Collecting information about letter {} from hard drive".format(
					marc_url.replace(MARC_URL, "")
				))
		
		OAI_PMH = et.parse("{}/{}.xml".format(
			marc_letters_folder_name,
			marc_url.replace(MARC_URL, "")
		)).getroot()
	else:
		if verbose:
			print("Collecting information about letter {} from server".format(
				marc_url.replace(MARC_URL, "")
			))
		
		response = requests.get(marc_url)
		OAI_PMH = et.fromstring(response.content)
	
	# navigate to the interesting part of the marc file
	metadata = OAI_PMH[2][0][1]
	
	record = metadata[0]
	
	fields = record.getchildren()
	
	# used to see if the author is not in field 700
	authorFound = False
	
	# used to see if the date is not in field 245
	dateFound = False
	
	# used for type of letter detection
	startsWithBriefAn = False
	
	# go through all fields to find the needed information
	for field in fields:
		if field.attrib:
			tagAttribute = field.attrib.get("tag")
			
			# get urn
			if tagAttribute == "024":
				if field.attrib.get("ind1") == "7":
					for subfield in field:
						if subfield.attrib.get("code") == "a":
							if not subfield.text:
								output["urn"] = "ERROR"
							else:
								output["urn"] = subfield.text
			
			# get author information
			if tagAttribute == "100":
				for subfield in field:
					# get author name
					if subfield.attrib.get("code") == "a":
						if subfield.text == "":
							authorFound = False
						else:
							output["author name"] = subfield.text
					
					# get author norm data
					elif subfield.attrib.get("code") == "0":
						
						output["author norm data"] = "{}{}".format(
							NORMDATA_URL,
							subfield.text.split(")")[1]
						)
			
			# get date information
			elif tagAttribute == "245":
				for subfield in field:
					
					# if the date is not here it might be in 264 c
					dateFound = True
					
					# see if we have a letter
					if subfield.attrib.get("code") == "a":
						if "[Brief an" in subfield.text:
							startsWithBriefAn = True
					
					elif subfield.attrib.get("code") == "b":
						# not testing for illegal date formats such as
						# 44.44.0001
						
						# a lot of times there is some extra text around date
						# so a regex should take care of that
						# I would still like to preserve the uncertain and
						# conjecture proprieties
						first_test = re.search(
							".*(\\[((\\d\\d.){2}\\d{4})(\\?)?\\]).*",
							subfield.text)
						
						if first_test:
							date = first_test.group(1)
						else:
							second_test = re.search(".*((\\d\\d.){2}\\d{4}).*",
							                        subfield.text)
							
							date = second_test.group(1) if second_test else ""
						
						if date == "":
							dateFound = False
						else:
							output["date"] = date
			
			elif tagAttribute == "264" and not dateFound:
				for subfield in field:
					if subfield.attrib.get("code") == "c":
						if subfield.text == "":
							output["date"] = ""
						else:
							# the possible "[xxxx?]" is handled as the marc
							# file is made
							output["date"] = subfield.text
			
			# letter can possibly be a draft
			elif tagAttribute == "500" and startsWithBriefAn:
				for subfield in field:
					if subfield.attrib.get("code") == "a":
						
						entwurfFound = re.search(".*[Ee]ntwurf.*",
						                         subfield.text)
						
						if entwurfFound:
							output["letter type"] = "Entwurf"
						else:
							output["letter type"] = "Brief"
			
			# get receiver information
			elif tagAttribute == "700":
				for subfield in field:
					if subfield.attrib.get("code") == "4":
						# aut means author
						
						if subfield.text == "aut" and not authorFound:
							for new_subfield in field:
								
								if new_subfield.attrib.get("code") == "a":
									output["author name"] = new_subfield.text
									authorFound = True
								
								elif new_subfield.attrib.get("code") == "0":
									output["author norm data"] = "{}{}".format(
										NORMDATA_URL,
										new_subfield.text.split(")")[1]
									)
						
						# rcp and oth are the receiver
						if subfield.text == "rcp" or subfield.text == "oth":
							for new_subfield in field:
								
								if new_subfield.attrib.get("code") == "a":
									output["receiver name"] = new_subfield.text
								
								# get receiver normdata
								elif new_subfield.attrib.get("code") == "0":
									output[
										"receiver norm data"] = "{}{}".format(
										NORMDATA_URL,
										new_subfield.text.split(")")[1]
									)
						
						# default is receiver
						elif output["receiver name"] == "Unknown":
							for new_subfield in field:
								
								if new_subfield.attrib.get("code") == "a":
									output["receiver name"] = new_subfield.text
								
								# get receiver normdata
								elif new_subfield.attrib.get("code") == "0":
									output[
										"receiver norm data"] = "{}{}".format(
										NORMDATA_URL,
										new_subfield.text.split(")")[1]
									)
			
			# get location information
			elif tagAttribute == "751":
				for subfield in field:
					# get location name
					if subfield.attrib.get("code") == "a":
						locationName = subfield.text
						
						output["location name"] = locationName
					
					# get location gndId
					elif subfield.attrib.get("code") == "0":
						locationGndID = subfield.text
						locationGndID = locationGndID.split(")")[1]
						locationGeonamesID = ""
						
						# get location normdata
						locationFound = True
						# see if the location is already in the dictionary
						if locationGndID in geo_names:
							locationGeonamesID = geo_names[locationGndID]
						
						# if not search using the wikidata API and save
						# the new data to the dictionary
						else:
							if locationGndID != "":
								locationGeonamesID = get_geonames_id(
									locationGndID, output["location name"])
							
							if locationGeonamesID == "":
								geo_names[locationGndID] = "unknown"
								locationFound = False
							else:
								geo_names[locationGndID] = locationGeonamesID
						
						# if a location has been found save it to the dict
						if locationFound and locationGeonamesID != "unknown":
							output["location norm data"] = "{}{}".format(
								GEONAMES_URL,
								locationGeonamesID
							)
						else:
							output["location norm data"] = ""
	
	return output


def add_letter_to_tei(marc_information: dict,
                      profile_desc: Any, root: Any, uuid_for_this_file: str):
	"""
	Adds a marc letter information to the specified tei collection file.
	
	:param marc_information: dictionary containing information of the marc file
	:param profile_desc: et.Element from where letter information can be added
	:param root: xml root element used for saving the xml file
	:param uuid_for_this_file: uuid of the tei collection file
	:returns: the tei file's tree so it can be saved
	"""
	
	# <correspDesc>
	
	# first see if the type is unwanted
	if removeType:
		# if so remove everything with sonstiges type
		if marc_information["letter type"] == "Sonstiges":
			if verbose:
				print("Letter type is Sonstiges => disregarding")
			
			return et.ElementTree(root)
		else:
			# allow everything else without the type argument
			correspDesc = et.SubElement(
				profile_desc,
				"correspDesc",
				ref=URN_URL + marc_information["urn"],
				source="#" + uuid_for_this_file
			)
	else:
		# if not set use the type field
		correspDesc = et.SubElement(
			profile_desc,
			"correspDesc",
			ref=URN_URL + marc_information["urn"],
			source="#" + uuid_for_this_file,
			type=marc_information["letter type"]
		)
	
	# <correspAction> author
	if marc_information["author name"] != "":
		correspAction = et.SubElement(correspDesc, "correspAction", type="sent")
		
		# <persName>
		# first check if ref is empty
		if not marc_information["author norm data"]:
			et.SubElement(
				correspAction, "persName"
			).text = marc_information["author name"]
		else:
			et.SubElement(
				correspAction, "persName",
				ref=marc_information["author norm data"]
			).text = marc_information["author name"]
		# </persName>
		
		if marc_information["date"] != "":
			# <date>
			# see if we have any conjecture "[]" evidence or uncertainty
			# "[?]" or none
			# also check every time if there is any wrong date and if so put
			# it in the text element
			
			date, textEntry = convert_date_to_iso(marc_information["date"])
			
			if "?]" in marc_information["date"]:
				dateObj = et.SubElement(
					correspAction, "date",
					when=date,
					evidence="conjecture",
					cert="low"
				)
			elif "]" in marc_information["date"]:
				dateObj = et.SubElement(
					correspAction, "date",
					when=date,
					evidence="conjecture"
				)
			elif "?" in marc_information["date"]:
				dateObj = et.SubElement(
					correspAction, "date",
					when=date,
					cert="low"
				)
			else:
				dateObj = et.SubElement(
					correspAction, "date",
					when=date
				)
			
			if textEntry:
				dateObj.text = textEntry
		# </date>
		
		# <placeName>
		if marc_information["location name"] != "":
			# first check if ref is empty
			if not marc_information["location norm data"]:
				et.SubElement(
					correspAction, "placeName",
				).text = marc_information["location name"]
			
			else:
				et.SubElement(
					correspAction, "placeName",
					ref=marc_information["location norm data"]
				).text = marc_information["location name"]
	
	# </placeName>
	
	# </correspAction> author
	
	# <correspAction> receiver
	if marc_information["receiver name"] != "":
		correspAction = et.SubElement(
			correspDesc,
			"correspAction",
			type="received"
		)
		
		# <persName>
		# first check if ref is empty
		if not marc_information["receiver norm data"]:
			et.SubElement(
				correspAction, "persName",
			).text = marc_information["receiver name"]
		else:
			et.SubElement(
				correspAction, "persName",
				ref=marc_information["receiver norm data"]
			).text = marc_information["receiver name"]
	# </persName>
	
	# </correspAction> receiver
	
	# </correspDesc>
	
	# </profileDesc>
	
	tree = et.ElementTree(root)
	
	return tree


def read_csv(file_name: str) -> dict:
	"""
	Reads a csv file and returns its information as a dictionary
	
	:param file_name: file name with the path as a string
	:returns: dictionary containing all information form the csv file
	"""
	
	input_file = csv.reader(open(file_name, "r", encoding="utf8"))
	
	output = {}
	for line in input_file:
		if len(line[1:]) > 1:
			temp = []
			for information in line[1:]:
				temp.append(information)
			output[line[0]] = temp
		else:
			output[line[0]] = line[1]
	
	return output


def save_csv(file_name: str, new_dict: dict):
	"""
	Overrides a csv file with the new dictionary information
	
	:param file_name: file name with the path as a string
	:param new_dict: the dictionary that will be used to override the file
	"""
	
	with open(file_name, 'w', encoding="utf8", newline='') as f:
		w = csv.writer(f)
		for item in new_dict.items():
			if type(item[1]) is list:
				w.writerow(tuple([item[0]] + item[1]))
			else:
				w.writerow(item)


def check_for_csv(file_name: str) -> dict:
	"""
	Checks to see if the given csv file exists and outputs its contents as a
	dict. If not it will output an empty dict
	
	:param file_name: file path of the csv file
	:returns: a dictionary containing the csv file information or an empty one
	"""
	output = {}
	if os.path.exists(file_name):
		output = read_csv(file_name)
	else:
		open(file_name, 'a').close()
	
	return output


def check_if_tei_file_exists(letter_collection_title: str,
                             uuid_dict: dict) -> Tuple[Any, Any, str]:
	"""
	Checks if the inputted tei file exists.
	
	If the file does not exist it will make one and will generate a new UUID.
	
	:param letter_collection_title: file name with the path as a string
	:param uuid_dict: dictionary containing all generated uuids
	:returns:
		(profileDesc) the element from where one needs to insert all
		marc data
		(root)
		the root element used for saving the tei file
		(uuids)
		the uuid of the tei file
	"""
	
	file_name = "{}".format(letter_collection_title.replace(" ", "_"))
	
	# see if the letter is already in the dictionary
	if file_name in uuidDict:
		uuid_for_this_file = uuid_dict[file_name]
	
	# if not create new uuid4 and add it to the dictionary
	else:
		uuid_for_this_file = str(uuid.uuid4())
		
		# UUID can not start with a digit
		while not uuid_for_this_file[0].isalpha():
			uuid_for_this_file = str(uuid.uuid4())
		
		uuid_dict[file_name] = uuid_for_this_file
	
	if os.path.exists(file_name):
		tree = et.parse(file_name)
		return tree.getroot()[0][-1], tree.getroot(), uuid_for_this_file
	else:
		
		# begin tei file
		root = et.Element("TEI", xmlns=TEI_URL)
		teiHeader = et.SubElement(root, "teiHeader")
		
		# <fileDesc>
		fileDesc = et.SubElement(teiHeader, "fileDesc")
		titleStmt = et.SubElement(fileDesc, "titleStmt")
		
		et.SubElement(titleStmt, "title").text = titleStmp_title
		
		# <editor>
		editor = et.SubElement(titleStmt, "editor")
		editor.text = author_of_file
		
		et.SubElement(editor, "email").text = email_of_author
		# </editor>
		
		# <publicationStmt>
		publicationStmt = et.SubElement(fileDesc, "publicationStmt")
		
		# <publisher>
		# TODO: change this to whatever need to be here
		publisherSubElement = et.SubElement(publicationStmt,
		                                    "publisher")
		et.SubElement(
			publisherSubElement,
			"ref",
			target=UB_URL
		).text = UB_NAME
		# </publisher>
		
		# TODO: the unknown should be a url pointing to the tei file just made
		et.SubElement(
			publicationStmt,
			"idno",
			type="url"
		).text = idno_url
		
		et.SubElement(
			publicationStmt,
			"date",
			when=datetime.datetime.now().isoformat()
		)
		
		# <availability>
		availability = et.SubElement(publicationStmt, "availability")
		et.SubElement(
			availability,
			"licence",
			target=LICENCE_URL
		).text = LICENCE_TEXT
		
		# </availability>
		
		# </publicationStmt>
		
		# <sourceDesc>
		sourceDesc = et.SubElement(fileDesc, "sourceDesc")
		
		# <bibl>
		bibl = et.SubElement(
			sourceDesc,
			"bibl",
			type="online",
			# id=uuid_for_this_file
		)
		
		bibl.attrib[
			"{http://www.w3.org/XML/1998/namespace}id"] = uuid_for_this_file
		
		# TODO: ADD BIBLIOGRAPHIC INFORMATION HERE
		bibl.text = "Briefsammlung Trew"
		
		et.SubElement(
			bibl,
			"ref",
			target=BRIEFSAMMLUNG_TREW_URL
		).text = BRIEFSAMMLUNG_TREW_URL
		
		# </bibl>
		
		# </sourceDesc>
		
		# </fileDesc>
		
		# <profileDesc>
		profileDesc = et.SubElement(teiHeader, "profileDesc")
		
		return profileDesc, root, uuid_for_this_file


def convert_marc_to_tei_bulk(letter_names: List[str],
                             letter_collection_title: str,
                             letter_collection_contents: dict,
                             geo_names: dict, uuids: dict):
	"""
	Converts marc letters and adds them to a collection tei file that is
	saved at the end.
	
	:param letter_names: a list of all the letter ids (BV...) to be added to
						the tei file
	:param letter_collection_title: name of the tei file to be used
	:param letter_collection_contents: dictionary containing all
									already added marc files
	:param geo_names: dictionary containing all already queried geonames
	:param uuids: dictionary containing all UUIDs already generated
	"""
	
	profileDesc, root, uuid_for_this_file = check_if_tei_file_exists(
		letter_collection_title,
		uuids
	)
	
	# in case something should go wrong just use the tree of the tei file again
	tree = et.ElementTree(root)
	
	# see if the tei collection file has already files in it and get them
	if letter_collection_title in letter_collection_contents:
		lettersAlreadyAdded = letter_collection_contents.get(
			letter_collection_title)
	else:
		lettersAlreadyAdded = []
	
	# make an array for the new to be added letters
	newLettersToAdd = []
	
	if verbose:
		for i, letter_name in enumerate(letter_names):
			
			if letter_name in lettersAlreadyAdded:
				print("Letter {} is already in the TEI file!\n".format(
					letter_name
				))
				continue
			else:
				newLettersToAdd.append(letter_name)
			
			marc_url = MARC_URL + letter_name
			
			marc_information_dict = get_information_from_marc(
				marc_url, geo_names
			)
			
			print("Letter {} out of {}".format(i + 1, len(letter_names)))
			
			print("{} in {}:\n".format(letter_name, letter_collection_title))
			
			print("Author name: {}".format(
				marc_information_dict["author name"]
			))
			
			print("Author norm data: {}".format(
				marc_information_dict["author norm data"]
			))
			
			print("Date: {}".format(marc_information_dict["date"]))
			
			print("Receiver name: {}".format(
				marc_information_dict["receiver name"]
			))
			
			print("Receiver norm data: {}".format(
				marc_information_dict["receiver norm data"]
			))
			
			print("Location name: {}".format(
				marc_information_dict["location name"]
			))
			
			print("Location norm data: {}\n".format(
				marc_information_dict["location norm data"]
			))
			
			tree = add_letter_to_tei(
				marc_information_dict,
				profileDesc, root, uuid_for_this_file
			)
	
	else:
		for i, letter_name in enumerate(tqdm(letter_names)):
			
			if letter_name in lettersAlreadyAdded:
				continue
			else:
				newLettersToAdd.append(letter_name)
			
			marc_url = MARC_URL + letter_name
			
			marc_information_dict = get_information_from_marc(
				marc_url, geo_names
			)
			
			tree = add_letter_to_tei(
				marc_information_dict,
				profileDesc, root, uuid_for_this_file
			)
	
	# print("ERROR\nProbably the letter you wanted to add does not " +
	# "exist\n")
	
	# add the new letters to the contents dict
	letter_collection_contents[letter_collection_title] = \
		[y for x in [lettersAlreadyAdded, newLettersToAdd] for y in x]
	
	# add <text><body><p/></body></text>
	root = tree.getroot()

# done in template
#	body = et.SubElement(root, "body")
#	text = et.SubElement(body, "text")
#	et.SubElement(text, "p")
	
	# save the file
	et.ElementTree(root).write(
		"{}".format(letter_collection_title.replace(" ", "_")),
		pretty_print=True,
		xml_declaration=True,
		encoding='UTF-8'
	)


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	
	parser.add_argument(
		"--input",
		"-i",
		help="set input txt file name containing the BV-IDs or a singe id "
		     + "that should be added"
	)
	
	parser.add_argument(
		"--output",
		"-o",
		help="set output file name"
	)
	
	parser.add_argument(
		"--marcfolder",
		"-mf",
		help="set marc files folder name"
	)
	
	parser.add_argument(
		"--removetype",
		"-rt",
		action='store_true',
		help="set this if you want the type to be removed and disregard any "
		     + "sonstige types"
	)
	
	parser.add_argument(
		"--verbose",
		"-v",
		action='store_true',
		help="set this if you want a verbose output"
	)
	
	args = parser.parse_args()
	
	letters = []
	if args.input:
		if ".txt" in args.input:
			letters = [line.rstrip('\n') for line in open(args.input)]
		else:
			letters = [args.input]
	else:
		print("Input file or letter id required!")
		exit()
	
	title_of_letter_collection = ""
	if args.output:
		title_of_letter_collection = args.output
	else:
		print("Output file required!")
		exit()
	
	if args.marcfolder:
		marc_letters_folder_name = args.marcfolder
	else:
		marc_letters_folder_name = "briefe"
	
	if args.removetype:
		removeType = True
	
	if args.verbose:
		verbose = True
	
	letters_on_drive = os.listdir("{}/".format(marc_letters_folder_name))
	letters_on_drive.sort()
	
	geoNamesDict = check_for_csv(GEONAMES_CSV_FILE_NAME)
	uuidDict = check_for_csv(UUID_CSV_FILE_NAME)
	letterCollectionContentsDict = check_for_csv(TEI_CONTENTS_CSV_FILE_NAME)
	
	convert_marc_to_tei_bulk(
		letters,
		title_of_letter_collection,
		letterCollectionContentsDict,
		geoNamesDict,
		uuidDict
	)
	
	save_csv(GEONAMES_CSV_FILE_NAME, geoNamesDict)
	save_csv(UUID_CSV_FILE_NAME, uuidDict)
	save_csv(TEI_CONTENTS_CSV_FILE_NAME, letterCollectionContentsDict)

# Comment the following line if you do not want to open the newly created
# file automatically for inspection
# os.startfile(title_of_letter_collection.replace(" ", "_"))
