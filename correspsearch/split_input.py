main_input = "bvnummern_sort.txt"

letters = [line.rstrip('\n') for line in open(main_input)]

supreme_output = []
new_output_array = []
for i, letter in enumerate(letters, start=1):
	new_output_array.append(letter)
	if i % 1000 == 0 or i == len(letters):
		supreme_output.append(new_output_array)
		new_output_array = []
		
for i, file_array in enumerate(supreme_output, start=1):
	with open("input{}.txt".format(i), "w") as text_file:
		for letter in file_array:
			text_file.write(letter + "\n")