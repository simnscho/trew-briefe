#!/bin/bash
# reads a list of bv numbers from stdin
# for each bv number, searches the place name and  gnd id in the corr. marc file

for BV in $(cat -)
do
  xmlstarlet sel -t -o "$BV:" -v '//*[@tag="751"]/*[@code="0"]' -o ':' -v '//*[@tag="751"]/*[@code="a"]' -n < marcxml/$BV.xml
done > bvnr_gnd_orte.txt
